﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCSender : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        NetworkController.getController().sendOSCMessage("RoverTable", "/position", "this is a message from RoverVR to RoverTable");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
