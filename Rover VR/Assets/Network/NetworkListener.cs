﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface NetworkListener {
    
    void messageArrived(string target, List<Network.OSCValue> values);

}
