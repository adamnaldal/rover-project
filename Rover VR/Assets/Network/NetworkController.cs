﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkController : MonoBehaviour {

    [System.Serializable]
    public class Client
    {
        public string name;
        public string ip;
        public int port = 3333;
    }

    private Network net;

    public int listenPort = 3333;
    public TUIOControl tuioController;
    public Client[] clients;
    public GameObject[] listeners;

    public void OnEnable()
    {
        net = new Network();
        net.connect(listenPort);
        foreach (Client client in clients)
        {
            net.addClient(client.name, client.ip, client.port);
        }

        foreach(GameObject obj in listeners)
        {
            MonoBehaviour[] list = obj.GetComponents<MonoBehaviour>();
            foreach(MonoBehaviour b in list) {
                if(b is NetworkListener)
                {
                    net.addListener((NetworkListener)b);
                }
            }
        }

        if(tuioController == null)
        {
            //No TUIOControl added to script, check if one is on this gameobject
            tuioController = GetComponent<TUIOControl>();
        }

        if(tuioController != null && tuioController.enabled)
        {
            Debug.Log("Setting TUIO: " + tuioController);
            net.setTuioControl(tuioController);
        }
    }

    public void addListener(NetworkListener listener)
    {
        net.addListener(listener);
    }

    public void removeListener(NetworkListener listener)
    {
        net.removeListener(listener);
    }

    private void OnDisable()
    {
        net.disconnect();
        net = null;
        cachedController = null;
    }

    public void Update()
    {
        net.runQueue();
    }

    public void sendOSCMessageToAll(string address, params object[] values)
    {
        net.sendMessageToAll(address, values);
    }

    public void sendOSCMessage(string receiver, string address, params object[] values)
    {
        net.sendMessage(receiver, address, values);
    }

    private static NetworkController cachedController = null;

    public static NetworkController getController()
    {
        if(cachedController == null)
        {
            cachedController = Object.FindObjectOfType<NetworkController>();
        }

        return cachedController;
    }
}
