﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class DebugOSCListener : MonoBehaviour, NetworkListener {
    public void messageArrived(string target, List<Network.OSCValue> values)
    {
        Debug.Log("Got OSC message from [" + target + "] Values: " + toString(values));
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private string toString(List<Network.OSCValue> list)
    {
        StringBuilder sb = new StringBuilder("");

        bool first = true;

        foreach(Network.OSCValue o in list)
        {
            if(first)
            {
                first = false;
            } else
            {
                sb.Append(", ");
            }

            sb.Append("["+(o.getRaw().GetType())+"] ").Append(o.getString());
        }

        return sb.ToString();
    }
}
