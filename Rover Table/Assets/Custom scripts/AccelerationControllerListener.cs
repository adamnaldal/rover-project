﻿using UnityEngine;
using System.Collections;
using TUIO;

public class AccelerationControllerListener : MovingAndRotatingObjectActionListener {
	private Transform parent;
	private GameObject haandtag;

	public void Start(){
		//parent = transform.parent;
		//Debug.Log (parent.name);
		haandtag = GameObject.Find("Acceleration Controller");
	}
	
	public override void updateTuioObject(TuioObject o){
		base.updateTuioObject(o);
		//Debug.Log(transform.position);
		// parent.GetComponent<Transform>().position = transform.localPosition;
		// parent.GetComponent<Transform>().rotation = transform.localRotation;
		haandtag.transform.localPosition = transform.localPosition;
		haandtag.transform.localRotation = transform.localRotation;
		
	}
}
