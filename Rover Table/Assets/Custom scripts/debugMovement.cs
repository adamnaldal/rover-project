using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugMovement : MonoBehaviour
{
    private Rigidbody m_Rigidbody;

    public float speed = 1f;
    public float rotateSpeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            m_Rigidbody.velocity = transform.forward * speed;
            Debug.Log("moving forward");
        }
        else
        {
            m_Rigidbody.velocity = transform.forward * 0;
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, rotateSpeed, 0);
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, -rotateSpeed, 0);
        }

    }
}

