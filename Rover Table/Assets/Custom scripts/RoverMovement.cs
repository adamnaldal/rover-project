using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoverMovement : MonoBehaviour
{
    public Quaternion receivedRotation;
    private GameObject directionController;
    private GameObject accelerationController;
    private Rigidbody m_Rigidbody;
    // Start is called before the first frame update
    private float prevAngle = 0;
    private float speed;
    private float speedFactor = 0.05f;
    void Start()
    {
        directionController = GameObject.Find("Direction Controller");
        accelerationController = GameObject.Find("Acceleration Controller");
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // changes direction of travel
        receivedRotation = directionController.transform.localRotation;
        transform.localRotation = receivedRotation;

        // changes speed
        float currentAngle = accelerationController.transform.eulerAngles.y;
        if (currentAngle > 180 && currentAngle <= 360f){
            speed = prevAngle * speedFactor;
        } else {
            speed = currentAngle * speedFactor;
            prevAngle = currentAngle;
        }
        
        m_Rigidbody.velocity = transform.forward * speed;
       
        NetworkController.getController().sendOSCMessage("RoverVR", "/roverPosition", transform.position.x,transform.position.z,transform.localRotation.eulerAngles.y);

    }
}
