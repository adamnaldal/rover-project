using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindField : MonoBehaviour
{
    public int density = 1;
    public float distanceWidth = 80;
    public float distanceHeight = 45;
    public GameObject prefab;
    public float arrowScale = 1;
    // Input data for our noise generator
    [SerializeField] public int width = 16;
    [SerializeField] public int height = 9;
    [SerializeField] public float scale;

    [SerializeField] public int octaves;
    [SerializeField] public float persistence;
    [SerializeField] public float lacunarity;

    [SerializeField] public int seed;
    [SerializeField] public Vector2 offset;

    [SerializeField] public MapType type = MapType.Noise;
    private Vector2[,] field;

    // Start is called before the first frame update
    void Start()
    {
        field = new Vector2[width * density, height * density];
        float[] magNoiseMap = NoiseMapGenerator.GenerateNoiseMap(width, height, seed, scale, octaves, persistence, lacunarity, offset);
        float[] dirNoiseMap = NoiseMapGenerator.GenerateNoiseMap(width, height, seed * 7, scale, octaves, persistence, lacunarity, offset);
        int x = 0;
        while (x < width * density)
        {
            int y = 0;
            while (y < height * density)
            {
                int magPointer = x * height + y;
                int dirPointer = x * height + y;

                field[x, y] = new Vector2(magNoiseMap[magPointer], dirNoiseMap[dirPointer]);
                //Debug.Log(field[x,y][0]);

                Vector3 pos = new Vector3(transform.position.x + x * distanceWidth / width, transform.position.y, transform.position.z + y * distanceHeight / height);
                Quaternion rot = Quaternion.Euler(0, field[x, y][0] * 360, 0);
                GameObject newArrow = Instantiate(prefab, pos, rot, transform);
                float arrowSize = Mathf.Clamp(field[x, y][1] * arrowScale, 0.2f, 1f);
                newArrow.transform.localScale = new Vector3(arrowSize, 0, arrowSize);
                y++;
            }
            x++;
        }

    }


    // Update is called once per frame
    void Update()
    {

    }
}
