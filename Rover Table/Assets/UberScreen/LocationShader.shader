﻿Shader "Hidden/LocationShader" {
	Properties {
		source ("Base (RGBA)", 2D) = "white" {}
		target ("Base (RGBA)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass {		
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
					
			CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma target 3.0
				#include "UnityCG.cginc"
	
				uniform sampler2D source;
				uniform sampler2D target;
				
				uniform float xPos;
				uniform float yPos;

				uniform float width;
				uniform float height;
				
				uniform float multisampleX;
				uniform float multisampleY;

				uniform int rotate90;
				
				uniform float translucency;
				uniform int clear;
				
				uniform bool flipX;
				uniform bool flipY;
	
				float4 frag (v2f_img i) : COLOR
				{					
					// Calculate position
					float2 coords;
					
					coords.x = -xPos/width + i.uv.x/width;
					coords.y = 1.0-(-yPos/height + i.uv.y/height);
					
					if (rotate90==1){
						//Rotate 90 degrees
						float temp = coords.x;
						coords.x = 1.0-coords.y;
						coords.y = temp;
					} else if (rotate90==2){
						//Rotate 180 degrees
						coords.x = 1.0-coords.x;
						coords.y = 1.0-coords.y;
					} else if (rotate90==3){
						//Rotate 270 degrees
						float temp = coords.x;
						coords.x = coords.y;
						coords.y = 1.0-temp;
					}
					
					// Flip X and Y
					if(flipX == 1) {
						coords.x = 1.0 - coords.x;
					}
					if(flipY == 1) {
						coords.y = 1.0 - coords.y;
					}
					

					// Check bounds
					float4 targetC = tex2D(target, i.uv);
					if (coords.x > 1.0 || coords.x < 0.0 || coords.y > 1.0 || coords.y < 0.0){
						return targetC;
					}

					// Get source colour
					float4 sourceC = tex2D(source, coords);
					if (multisampleX>0){
					    sourceC += 0.5*tex2D(source, coords+fixed2(-multisampleX,-multisampleY));
					    sourceC += 0.5*tex2D(source, coords+fixed2(0,-multisampleY));
					    sourceC += 0.5*tex2D(source, coords+fixed2(multisampleX,-multisampleY));

					    sourceC += 0.5*tex2D(source, coords+fixed2(-multisampleX,0));
					    sourceC += 0.5*tex2D(source, coords+fixed2(multisampleX,0));

					    sourceC += 0.5*tex2D(source, coords+fixed2(-multisampleX,multisampleY));
					    sourceC += 0.5*tex2D(source, coords+fixed2(0,multisampleY));
					    sourceC += 0.5*tex2D(source, coords+fixed2(multisampleX,multisampleY));
					    sourceC /= 5;
					}

					// Apply translucency
					sourceC.a *= (1-translucency);
					sourceC.a = clamp(sourceC.a, 0, 1);
					
					// Alphablend onto target
					if (clear==1) {
						return float4(sourceC.rgb*(1-translucency), 1);
					} else {
						return (1-sourceC.a) * targetC + sourceC.a * sourceC;					
					}
				}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
