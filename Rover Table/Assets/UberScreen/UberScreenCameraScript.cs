using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.InteropServices;

[RequireComponent(typeof(Camera))]
public class UberScreenCameraScript : MonoBehaviour
{
    public Shader locationShader;
    private Material material;
    private RenderTexture targetTexture;
    private RenderTexture temporaryTexture;
    private RenderTexture emptyTexture;
    public int totalWidth = 1920;
    public int totalHeight = 1200;
    public int windowXPos = 1920;
    public int windowYPos = 0;
    public bool useEditorGameWindow = false;
    public bool doFrameGrabbing = false;
    public int captureFramerate = 30;

    public string windowConfig = "";

    public enum RotationDirection { None, Rotate90, Rotate180, Rotate270 };

    [System.Serializable]
    public class InputDefinition
    {
        public string name;
        public RenderTexture texture;
        public bool flipX = false;
        public bool flipY = false;
        public bool clearBG = true;
        public float xPos = 0;
        public float yPos = 0;
        public RotationDirection rotation = RotationDirection.None;
        public float width = 1.0f;
        public float height = 1.0f;
        public float translucency = 0;
        public bool multisampling = false;
    }
    public InputDefinition[] inputs;

#if UNITY_EDITOR
    private FullscreenEditorWindow window = null;
#endif

    [DllImport("user32.dll")]
    static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();
    [DllImport("user32.dll")]
    static extern IntPtr GetActiveWindow();
    [DllImport("user32.dll", EntryPoint = "SetWindowLong", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);
    [DllImport("user32.dll", EntryPoint = "GetWindowLong", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern long GetWindowLong(IntPtr hWnd, int nIndex);

    void OnEnable()
    {
        loadFromXml();
        locationShader = Shader.Find("Hidden/LocationShader");
        setupEmptyTexture();

        if(doFrameGrabbing)
        {
            Time.captureFramerate = captureFramerate;
        }

        if (!useEditorGameWindow)
        {
#if UNITY_EDITOR
            if (window == null)
            {
                window = ScriptableObject.CreateInstance<FullscreenEditorWindow>();
                window.setUberScript(this);
                window.ShowPopup();
            }
#else
                // Use standard out
                GetComponent<Camera>().depth = 100;
                Debug.Log("Launching compositor in Executable mode (no plugin)");
#endif
            setWindowLocation();
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null)
        {
            material = new Material(locationShader);
            material.hideFlags = HideFlags.HideAndDontSave;
        }
        if (targetTexture == null || targetTexture.width != totalWidth || targetTexture.height != totalHeight)
        {
            onTargetTextureChanged();
        }

        // Clear hack
        Graphics.Blit(source, destination);
        Graphics.Blit(emptyTexture, targetTexture);

        foreach (InputDefinition input in inputs)
        {
            if (input.texture != null)
            {
                Graphics.Blit(targetTexture, temporaryTexture);
                material.SetFloat("xPos", input.xPos);
                material.SetFloat("yPos", input.yPos);
                material.SetInt("rotate90", getRotate90FromEnum(input.rotation));
                material.SetFloat("width", input.width);
                material.SetFloat("height", input.height);
                material.SetInt("clear", input.clearBG ? 1 : 0);
                material.SetFloat("translucency", input.translucency);
                material.SetInt("flipX", input.flipX ? 1 : 0);
                material.SetInt("flipY", input.flipY ? 1 : 0);
                material.SetTexture("source", input.texture);
                material.SetTexture("target", temporaryTexture);

                float multiDist = 0.4f;
                if (input.multisampling)
                {
                    float multiX = multiDist / targetTexture.width / input.width;
                    float multiY = multiDist / targetTexture.height / input.height;
                    if (input.rotation == RotationDirection.Rotate90 || input.rotation == RotationDirection.Rotate270)
                    {
                        material.SetFloat("multisampleX", multiY);
                        material.SetFloat("multisampleY", multiX);
                    }
                    else
                    {
                        material.SetFloat("multisampleX", multiX);
                        material.SetFloat("multisampleY", multiY);
                    }
                }
                else
                {
                    material.SetFloat("multisampleX", 0);
                    material.SetFloat("multisampleY", 0);
                }


                if (doFrameGrabbing && Application.isPlaying)
                {
                    RenderTexture saved = RenderTexture.active;
                    RenderTexture.active = input.texture;

                    Texture2D tex = new Texture2D(input.texture.width, input.texture.height, TextureFormat.RGB24, false);
                    tex.ReadPixels(new Rect(0, 0, input.texture.width, input.texture.height), 0, 0);
                    tex.Apply();

                    byte[] pixels = ImageConversion.EncodeToJPG(tex, 95);

					string dir = "grabbed/"+input.name;
                    string location = dir + "/" + Time.frameCount + ".jpg";

					System.IO.Directory.CreateDirectory(dir);
					
                    System.IO.File.WriteAllBytes(location, pixels);

                    Destroy(tex);

                    RenderTexture.active = saved;
                }

                Graphics.Blit(input.texture, targetTexture, material);
            }
        }

        // Flip everything since Unity3D is silly by default
        Graphics.Blit(targetTexture, temporaryTexture);
        material.SetFloat("xPos", 0f);
        material.SetFloat("yPos", 0f);
        material.SetInt("rotate90", 0);
        material.SetFloat("width", 1f);
        material.SetFloat("height", 1f);
        material.SetInt("clear", 1);
        material.SetFloat("translucency", 0f);
        material.SetInt("flipX", 0);
        material.SetInt("flipY", 0);
        material.SetFloat("multisampleX", 0);
        material.SetFloat("multisampleY", 0);
        material.SetTexture("source", temporaryTexture);
        material.SetTexture("target", temporaryTexture);
#if UNITY_EDITOR
        if (useEditorGameWindow)
        {
            Graphics.Blit(temporaryTexture, destination, material);
        }
        else
        {
            Graphics.Blit(temporaryTexture, targetTexture, material);
        }
#else
        Graphics.Blit (temporaryTexture, destination, material);
#endif

        //Do rendering?
        if (!useEditorGameWindow)
        {
#if UNITY_EDITOR
            if (window != null) window.Repaint();
#endif
        }
    }

    protected void setWindowLocation()
    {
        if (!useEditorGameWindow)
        {
#if UNITY_EDITOR
            if (window != null) window.position = new Rect(windowXPos, windowYPos, totalWidth, totalHeight);
#else
                Debug.Log("Scaling exe-mode window");
                const long WS_BORDER = 0x00800000L;
                const long WS_DLGFRAME = 0x00400000L;
                const long WS_THICKFRAME = 0x00040000L;
                const int GWL_STYLE = -16;
                const uint SWP_SHOWWINDOW = 0x0040;

                IntPtr hWnd=GetActiveWindow ();
                //IntPtr hWnd=GetForegroundWindow ();
                if (hWnd != IntPtr.Zero){
                    long old_style = GetWindowLong(hWnd,GWL_STYLE);
                    SetWindowLong(hWnd,GWL_STYLE,old_style & ~WS_BORDER & ~WS_THICKFRAME & ~WS_DLGFRAME);
                    SetWindowPos(hWnd, 0, windowXPos, windowYPos, totalWidth, totalHeight, SWP_SHOWWINDOW);// | SWP_FRAMECHANGED);
                } else {
                    Debug.Log("Big Warning: Skipped setting window parameters because no window from our thread had focus");
                }
#endif
        }
    }

    private void setupEmptyTexture()
    {
        //Create EmptyTexture
        emptyTexture = new RenderTexture(64, 64, 24);
        emptyTexture.Create();

        //Clear emptyTexture
        RenderTexture oldActive = RenderTexture.active;
        RenderTexture.active = emptyTexture;
        GL.Clear(true, true, Color.black);
        RenderTexture.active = oldActive;
    }

    void onTargetTextureChanged()
    {
        targetTexture = new RenderTexture(totalWidth, totalHeight, 24);
        temporaryTexture = new RenderTexture(totalWidth, totalHeight, 24);
        targetTexture.Create();
        temporaryTexture.Create();

#if UNITY_EDITOR
        if (!useEditorGameWindow) GetComponent<Camera>().targetTexture = targetTexture;
        if (window != null) window.setTargetTexture(targetTexture);
#endif
    }

    void OnDisable()
    {
        Time.captureFramerate = 0;

        if (targetTexture != null)
        {
            if (!useEditorGameWindow)
            {
                GetComponent<Camera>().targetTexture = null;
            }
            targetTexture.Release();
            DestroyImmediate(targetTexture);
            targetTexture = null;
        }
        if (material)
        {
            DestroyImmediate(material);
            material = null;
        }
#if UNITY_EDITOR
        if (window != null)
        {
            window.Close();
            window = null;
        }
#endif
    }

    private int getRotate90FromEnum(RotationDirection rotation)
    {
        switch (rotation)
        {
            case RotationDirection.None:
                return 0;
            case RotationDirection.Rotate90:
                return 1;
            case RotationDirection.Rotate180:
                return 2;
            case RotationDirection.Rotate270:
                return 3;
        }

        return 0;
    }

    private void loadFromXml()
    {
#if !UNITY_EDITOR
        if (File.Exists (windowConfig)) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(windowConfig);
            
            XmlNode docRoot = xmlDoc.DocumentElement;

            windowXPos = int.Parse(docRoot.SelectNodes("/uberscreen/window/x").Item(0).FirstChild.Value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            windowYPos = int.Parse(docRoot.SelectNodes("/uberscreen/window/y").Item(0).FirstChild.Value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            totalWidth = int.Parse(docRoot.SelectNodes("/uberscreen/window/width").Item(0).FirstChild.Value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            totalHeight = int.Parse(docRoot.SelectNodes("/uberscreen/window/height").Item(0).FirstChild.Value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        }
#endif
    }

#if UNITY_EDITOR
    public List<EditorWindowMouseListener> editorWindowMouseListeners = new List<EditorWindowMouseListener>();
#endif
}

// Non-plugin fullscreen editor window
#if UNITY_EDITOR
public interface EditorWindowMouseListener
{
    void mouseDown(int button, Vector2 position);
    void mouseUp(int button, Vector2 position);
}

public class FullscreenEditorWindow : EditorWindow
{

    private Material uiMaterial;
    private FullscreenEditorWindow window;
    private RenderTexture targetTexture = null;
    private UberScreenCameraScript uberScript = null;

    public FullscreenEditorWindow()
    {

    }

    public void setUberScript(UberScreenCameraScript script)
    {
        this.uberScript = script;
    }

    public void setTargetTexture(RenderTexture target)
    {
        targetTexture = target;
    }

    public void OnEnable()
    {
        hideFlags = HideFlags.DontSave;
        uiMaterial = new Material(Shader.Find("Unlit/Texture"));
        uiMaterial.hideFlags = HideFlags.HideAndDontSave;
    }

    void OnGUI()
    {
        if (uberScript != null)
        {
            Event e = Event.current;

            Vector2 position = e.mousePosition;
            int button = e.button;
            switch (e.type)
            {
                case EventType.KeyDown:
                    if (e.shift && e.keyCode == KeyCode.Escape)
                    {
                        this.Close();
                    }
                    break;
                case EventType.MouseDown:
                    foreach (EditorWindowMouseListener listener in uberScript.editorWindowMouseListeners)
                    {
                        listener.mouseDown(button, position);
                    }
                    break;
                case EventType.MouseUp:
                    foreach (EditorWindowMouseListener listener in uberScript.editorWindowMouseListeners)
                    {
                        listener.mouseUp(button, position);
                    }
                    break;
                default:
                    //Unhandled eventType
                    break;
            }
        }

        if (targetTexture != null) EditorGUI.DrawPreviewTexture(new Rect(0, 0, this.position.width, this.position.height), targetTexture, uiMaterial, ScaleMode.StretchToFill);
    }
}
#endif
